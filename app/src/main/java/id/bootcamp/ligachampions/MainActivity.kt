package id.bootcamp.ligachampions

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private lateinit var rvClubs: RecyclerView
    private val clubList = ArrayList<Club>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvClubs = findViewById(R.id.rvKlub)
        clubList.addAll(getAllClubs())
        showRecyclerList()
        val fab = findViewById<FloatingActionButton>(R.id.fabAbout)

        fab.setOnClickListener{
            val intent = Intent(this, AboutActivity::class.java)
            startActivity(intent)
        }
    }

    private fun showRecyclerList() {
        rvClubs.layoutManager = LinearLayoutManager(this)
        val listClubAdapter = ListClubAdapter(clubList)
        rvClubs.adapter = listClubAdapter

        listClubAdapter.setOnClickCallBack (object: ListClubAdapter.OnItemClickCallBack {
            override fun onItemClicked (data: Club) {
                //Toast.makeText(this@MainActivity, "Klub Anda ${data.name}", Toast.LENGTH_SHORT).show()
                val intent = Intent(this@MainActivity, TahunJuara::class.java)
                intent.putExtra("data", data)
                startActivity(intent)
            }
        })
    }

    private fun getAllClubs(): ArrayList<Club> {
        val dataClub = resources.getStringArray(R.array.data_name)
        val dataBanyak = resources.getStringArray(R.array.banyak_juara)
        val dataLogo = resources.obtainTypedArray(R.array.data_logo)
        val dataJuara = resources.getStringArray(R.array.data_juara)
        val listClub = ArrayList<Club>()//[hero1, hero2...]

        for (i in 0..dataClub.lastIndex) { //0
            val club = Club(dataClub[i], dataBanyak[i], dataLogo.getResourceId(i, -1), dataJuara[i])
            listClub.add(club)
        }
        dataLogo.recycle()
        return listClub
    }
}