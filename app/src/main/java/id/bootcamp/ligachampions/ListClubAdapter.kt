package id.bootcamp.ligachampions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListClubAdapter(private var listClub: ArrayList<Club>): RecyclerView.Adapter<ListClubAdapter.ListViewHolder>() {
    private lateinit var onItemClickCallBack: OnItemClickCallBack

    interface OnItemClickCallBack {
        fun onItemClicked(data: Club)
    }

    fun setOnClickCallBack(onItemClickCallBack: OnItemClickCallBack) {
        this.onItemClickCallBack = onItemClickCallBack
    }

    class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val imgLogo = itemView.findViewById<ImageView>(R.id.logoKlub)
        val textClub = itemView.findViewById<TextView>(R.id.tvNamaKlub)
        val textChamp = itemView.findViewById<TextView>(R.id.tvTahunJuara)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.activity_klub_juara, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listClub.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val dataList = listClub[position]
        holder.imgLogo.setImageResource(dataList.logo)
        holder.textClub.text = dataList.name
        holder.textChamp.text = dataList.banyakJuara

        holder.itemView.setOnClickListener {
//            Toast.makeText(holder.itemView.context, "Anda memilih ${data.name}!", Toast.LENGTH_SHORT).show()
            onItemClickCallBack.onItemClicked(dataList)
        }
    }
}