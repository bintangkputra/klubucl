package id.bootcamp.ligachampions

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Club(
    val name: String,
    val banyakJuara: String,
    val logo: Int,
    val tahunJuara: String
): Parcelable
