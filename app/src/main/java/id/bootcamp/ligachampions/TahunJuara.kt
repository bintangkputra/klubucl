package id.bootcamp.ligachampions

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class TahunJuara : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tahun_juara)

        val klubData = intent.getParcelableExtra<Club>("data")
        val klub = findViewById<ImageView>(R.id.ivklubJuara)
        val namaKLub = findViewById<TextView>(R.id.tvKlubUCL)
        val tahunJuara = findViewById<TextView>(R.id.tvTahunJuaraKLub)

        namaKLub.text = klubData?.name
        klub.setImageResource(klubData?.logo?: 0)
        tahunJuara.text = klubData?.tahunJuara

        klub.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}